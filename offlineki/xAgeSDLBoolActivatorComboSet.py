# -*- coding: utf-8 -*-
""" *==LICENSE==*

CyanWorlds.com Engine - MMOG client, server and tools
Copyright (C) 2011  Cyan Worlds, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Additional permissions under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or
combining it with any of RAD Game Tools Bink SDK, Autodesk 3ds Max SDK,
NVIDIA PhysX SDK, Microsoft DirectX SDK, OpenSSL library, Independent
JPEG Group JPEG library, Microsoft Windows Media SDK, or Apple QuickTime SDK
(or a modified version of those libraries),
containing parts covered by the terms of the Bink SDK EULA, 3ds Max EULA,
PhysX SDK EULA, DirectX SDK EULA, OpenSSL and SSLeay licenses, IJG
JPEG Library README, Windows Media SDK EULA, or QuickTime SDK EULA, the
licensors of this Program grant you additional
permission to convey the resulting work. Corresponding Source for a
non-source form of such a combination shall include the source code for
the parts of OpenSSL and IJG JPEG Library used as well as that of the covered
work.

You can contact Cyan Worlds, Inc. by email legal@cyan.com
 or by snail mail at:
      Cyan Worlds, Inc.
      14617 N Newport Hwy
      Mead, WA   99021

 *==LICENSE==* """

from Plasma import *
from PlasmaTypes import *

solvedVariableName = ptAttribString(1, "SDL: Boolean Solved Variable")
butsInUseVariableName = ptAttribString(2, "SDL: Button Lock Variable")
numCorrectVariableName = ptAttribString(3, "SDL: Number of correct entries")
combination = ptAttribString(4, "Combination")
resetOnEmpty = ptAttribBoolean(5, "Reset when the Age shuts down", default=0)
disableOnSolve = ptAttribBoolean(6, "Disable activators on solve", default=1)
actButtons = ptAttribActivatorList(7, "Act: Buttons")
respButtonPush = ptAttribResponder(8, "Resp: Button Push")

class xAgeSDLBoolActivatorComboSet(ptResponder, object):
    def __init__(self):
        ptResponder.__init__(self)
        self.version = 1
        self.id = 719

        # Defer setting up the handlers until PFM is initialized by the engine
        self._NotifyHandlers = {}
        self._HitActId = None

    def OnInit(self):
        self._ValidateCombination()
        if respButtonPush.value is not None:
            self._NotifyHandlers[actButtons.id] = self._RunPushResp
        else:
            self._NotifyHandlers[actButtons.id] = self._PickButton
        self._NotifyHandlers[respButtonPush.id] = self._RespPushComplete

    def OnServerInitComplete(self):
        ageSDL = PtGetAgeSDL()
        ageSDL.setFlags(solvedVariableName.value, 1, 1)
        ageSDL.sendToClients(solvedVariableName.value)
        ageSDL.setNotify(self.key, solvedVariableName.value, 0.0)
        ageSDL.setFlags(butsInUseVariableName.value, 1, 1)
        ageSDL.sendToClients(butsInUseVariableName.value)
        ageSDL.setNotify(self.key, butsInUseVariableName.value, 0.0)
        ageSDL.setFlags(numCorrectVariableName.value, 1, 1)
        ageSDL.sendToClients(numCorrectVariableName.value)

        if not PtGetPlayerList():
            self._butsInUse = 0
            if resetOnEmpty.value:
                self._solved = (0, "fastforward")
                self._numCorrect = (0, "fastforward")

        if self._butsInUse:
            actButtons.disable()

    # ======================================================================

    def OnSDLNotify(self, VARname, SDLname, playerID, tag):
        if VARname == solvedVariableName.value:
            if not self._solved:
                self._numCorrect = 0
        elif VARname == butsInUseVariableName.value:
            if self._butsInUse:
                actButtons.disable()
            else:
                actButtons.enable()

    def OnNotify(self, state, id, events):
        if not state or not PtWasLocallyNotified(self.key):
            return
        handler = self._NotifyHandlers.get(id, None)
        if handler is None:
            PtDebugPrint("xAgeSDLBoolActComboSet.OnNotify():\tUnhandled notify id=%d events=%s" % (id, events), level=kDebugDumpLevel)
        else:
            handler(events)

    # ======================================================================

    def _GetAgeSDL(self, attr):
        return PtGetAgeSDL()[attr.value][0]
    def _SetAgeSDL(self, attr, value):
        ageSDL = PtGetAgeSDL()
        if isinstance(value, tuple):
            state, hint = value
        else:
            state, hint = value, ""
        ageSDL.setTagString(attr.value, hint)
        ageSDL.setIndexNow(attr.value, 0, state)

    def _get_butsInUse(self):
        return self._GetAgeSDL(butsInUseVariableName)
    def _set_butsInUse(self, value):
        self._SetAgeSDL(butsInUseVariableName, value)
    _butsInUse = property(_get_butsInUse, _set_butsInUse)

    def _get_solved(self):
        return self._GetAgeSDL(solvedVariableName)
    def _set_solved(self, value):
        self._SetAgeSDL(solvedVariableName, value)
    _solved = property(_get_solved, _set_solved)

    def _get_numCorrect(self):
        return self._GetAgeSDL(numCorrectVariableName)
    def _set_numCorrect(self, value):
        self._SetAgeSDL(numCorrectVariableName, value)
    _numCorrect = property(_get_numCorrect, _set_numCorrect)

    # ======================================================================

    def _GetPickedActID(self, events):
        for i in events:
            if i[0] != kPickedEvent:
                continue
            pickedSOKey = i[3].getKey()
            for j in xrange(len(actButtons.value)):
                if actButtons.value[j].getParentKey() == pickedSOKey:
                    return j
        # Still here - error
        PtDebugPrint("xAgeSDLBoolActivatorComboSet._GetPickedActID():\tUnable to determine picked activator ID")
        return None

    def _PickButton(self, events):
        # Run if there is no push responder
        actId = self._GetPickedActID(events)
        PtDebugPrint("xAgeSDLBoolActivatorComboSet._PickedButton():\tPicked button %d" % actId, level=kWarningLevel)
        self._TriggerButton(actId)

    def _RunPushResp(self, events):
        self._HitActId = self._GetPickedActID(events)
        PtDebugPrint("xAgeSDLBoolActivatorComboSet._RunPushResp():\tPushing button %d" % self._HitActId, level=kWarningLevel)
        self._butsInUse = 1

        # Responders can't be fired by integer index, so we'll have to do this the hard way...
        notify = ptNotify(self.key)
        if isinstance(respButtonPush.value, (list, tuple)):
            for i in respButtonPush.value:
                notify.addReceiver(i)
        else:
            notify.addReceiver(respButtonPush.value)
        notify.netPropagate(1)
        notify.netForce(1)
        avKey = PtGetLocalAvatar().getKey()
        notify.addCollisionEvent(1, avKey, avKey)
        notify.setActivate(1.0)
        notify.addResponderState(self._HitActId)
        # Whoosh... off it goes...
        notify.send()

    def _RespPushComplete(self, events):
        self._butsInUse = 0

        # Proxy over to the "wait, there is no responder" method
        self._TriggerButton(self._HitActId)
        self._HitActId = None

    def _TriggerButton(self, actId):
        if self._solved:
            PtDebugPrint("xAgeSDLBoolActComboSet._TriggerButton():\tYou just unsolved it, moron.", level=kWarningLevel)
            self._solved = 0
            return

        try:
            desiredActId = self._combination[self._numCorrect]
        except IndexError:
            PtDebugPrint("xAgeSDLBoolActComboSet._TriggerButton():\tWoah, an index error... Let's pretend this is a bad input...")
            desiredActId = -1

        if desiredActId == actId:
            PtDebugPrint("xAgeSDLBoolActComboSet._TriggerButton():\tThat's right!", level=kWarningLevel)
            self._numCorrect += 1
            if self._numCorrect == len(self._combination):
                PtDebugPrint("xAgeSDLBoolActComboSet._TriggerButton():\tWoohoo! You have solved the puzzle!", level=kWarningLevel)
                self._solved = 1
                if disableOnSolve.value:
                    self._butsInUse = 1
        else:
            PtDebugPrint("xAgeSDLBoolActComboSet._TriggerButton():\tWRONG! THAT'S ***WRONG***!", level=kWarningLevel)
            self._solved = 0
            self._numCorrect = 0

    def _ValidateCombination(self):
        if not combination.value:
            PtDebugPrint("xAgeSDLBoolActComboSet._ValidateCombination():\tCombination is unset")
        if ',' not in combination.value:
            PtDebugPrint("xAgeSDLBoolActComboSet._ValidateCombination():\tCombination does not have separation delimiters")
        self._combination = [int(i) for i in combination.value.split(',')]


## Copy'pasta' glue from plasma/glue.py for TPotS. Not needed for Korman.
glue_cl = None
glue_inst = None
glue_params = None
glue_paramKeys = None
try:
    x = glue_verbose
except NameError:
    glue_verbose = 0

def glue_getClass():
    global glue_cl
    if (glue_cl == None):
        try:
            cl = eval(glue_name)
            if issubclass(cl, ptModifier):
                glue_cl = cl
            elif glue_verbose:
                print ('Class %s is not derived from modifier' % cl.__name__)
        except NameError:
            if glue_verbose:
                try:
                    print ('Could not find class %s' % glue_name)
                except NameError:
                    print 'Filename/classname not set!'
    return glue_cl


def glue_getInst():
    global glue_inst
    if (type(glue_inst) == type(None)):
        cl = glue_getClass()
        if (cl != None):
            glue_inst = cl()
    return glue_inst


def glue_delInst():
    global glue_inst
    global glue_cl
    global glue_paramKeys
    global glue_params
    if (type(glue_inst) != type(None)):
        del glue_inst
    glue_cl = None
    glue_params = None
    glue_paramKeys = None


def glue_getVersion():
    inst = glue_getInst()
    ver = inst.version
    glue_delInst()
    return ver


def glue_findAndAddAttribs(obj, glue_params):
    if isinstance(obj, ptAttribute):
        if glue_params.has_key(obj.id):
            if glue_verbose:
                print 'WARNING: Duplicate attribute ids!'
                print ('%s has id %d which is already defined in %s' %
                      (obj.name, obj.id, glue_params[obj.id].name))
        else:
            glue_params[obj.id] = obj
    elif type(obj) == type([]):
        for o in obj:
            glue_findAndAddAttribs(o, glue_params)
    elif type(obj) == type({}):
        for o in obj.values():
            glue_findAndAddAttribs(o, glue_params)
    elif type(obj) == type(()):
        for o in obj:
            glue_findAndAddAttribs(o, glue_params)


def glue_getParamDict():
    global glue_paramKeys
    global glue_params
    if type(glue_params) == type(None):
        glue_params = {}
        gd = globals()
        for obj in gd.values():
            glue_findAndAddAttribs(obj, glue_params)
        glue_paramKeys = glue_params.keys()
        glue_paramKeys.sort()
        glue_paramKeys.reverse()
    return glue_params


def glue_getClassName():
    cl = glue_getClass()
    if (cl != None):
        return cl.__name__
    if glue_verbose:
        print ('Class not found in %s.py' % glue_name)
    return None


def glue_getBlockID():
    inst = glue_getInst()
    if (inst != None):
        return inst.id
    if glue_verbose:
        print ('Instance could not be created in %s.py' % glue_name)
    return None


def glue_getNumParams():
    pd = glue_getParamDict()
    if (pd != None):
        return len(pd)
    if glue_verbose:
        print ('No attributes found in %s.py' % glue_name)
    return 0


def glue_getParam(number):
    pd = glue_getParamDict()
    if (pd != None):
        if type(glue_paramKeys) == type([]):
            if (number >= 0) and (number < len(glue_paramKeys)):
                return pd[glue_paramKeys[number]].getdef()
            else:
                print ('glue_getParam: Error! %d out of range of attribute list' % number)
        else:
            pl = pd.values()
            if (number >= 0) and (number < len(pl)):
                return pl[number].getdef()
            elif glue_verbose:
                print ('glue_getParam: Error! %d out of range of attribute list' % number)
    if glue_verbose:
        print 'GLUE: Attribute list error'
    return None


def glue_setParam(id, value):
    pd = glue_getParamDict()
    if (pd != None):
        if pd.has_key(id):
            try:
                pd[id].__setvalue__(value)
            except AttributeError:
                if isinstance(pd[id], ptAttributeList):
                    try:
                        if type(pd[id].value) != type([]):
                            pd[id].value = []
                    except AttributeError:
                        pd[id].value = []
                    pd[id].value.append(value)
                else:
                    pd[id].value = value
        elif glue_verbose:
            print "setParam: can't find id=", id
    else:
        print 'setParma: Something terribly has gone wrong. Head for the cover.'


def glue_isNamedAttribute(id):
    pd = glue_getParamDict()
    if (pd != None):
        try:
            if isinstance(pd[id], ptAttribNamedActivator):
                return 1
            if isinstance(pd[id], ptAttribNamedResponder):
                return 2
        except KeyError:
            if glue_verbose:
                print ('Could not find id=%d attribute' % id)
    return 0


def glue_isMultiModifier():
    inst = glue_getInst()
    if isinstance(inst, ptMultiModifier):
        return 1
    return 0


def glue_getVisInfo(number):
    pd = glue_getParamDict()
    if pd != None:
        if type(glue_paramKeys) == type([]):
            if (number >= 0) and (number < len(glue_paramKeys)):
                return pd[glue_paramKeys[number]].getVisInfo()
            else:
                print ('glue_getVisInfo: Error! %d out of range of attribute list' % number)
        else:
            pl = pd.values()
            if (number >= 0) and (number < len(pl)):
                return pl[number].getVisInfo()
            elif glue_verbose:
                print ('glue_getVisInfo: Error! %d out of range of attribute list' % number)
    if glue_verbose:
        print 'GLUE: Attribute list error'
    return None
