# -*- coding: utf-8 -*-
""" *==LICENSE==*

CyanWorlds.com Engine - MMOG client, server and tools
Copyright (C) 2011  Cyan Worlds, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Additional permissions under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or
combining it with any of RAD Game Tools Bink SDK, Autodesk 3ds Max SDK,
NVIDIA PhysX SDK, Microsoft DirectX SDK, OpenSSL library, Independent
JPEG Group JPEG library, Microsoft Windows Media SDK, or Apple QuickTime SDK
(or a modified version of those libraries),
containing parts covered by the terms of the Bink SDK EULA, 3ds Max EULA,
PhysX SDK EULA, DirectX SDK EULA, OpenSSL and SSLeay licenses, IJG
JPEG Library README, Windows Media SDK EULA, or QuickTime SDK EULA, the
licensors of this Program grant you additional
permission to convey the resulting work. Corresponding Source for a
non-source form of such a combination shall include the source code for
the parts of OpenSSL and IJG JPEG Library used as well as that of the covered
work.

You can contact Cyan Worlds, Inc. by email legal@cyan.com
 or by snail mail at:
      Cyan Worlds, Inc.
      14617 N Newport Hwy
      Mead, WA   99021

 *==LICENSE==* """

from Plasma import *
from PlasmaTypes import *
from PlasmaConstants import *

actRegionSensor = ptAttribActivator(1, "act: Camera Region Sensor")
camera = ptAttribSceneobject(2, "Entry Camera")
undoFirstPerson = ptAttribBoolean(3, "Override the player's first person camera setting?", default=True)

kDisableRegionTimer = 1

class xEntryCam(ptModifier):
    def __init__(self):
        ptModifier.__init__(self)
        self.id = 900378302
        self.version = 1

        self._entryCamActive = False
        self._linkedIn = False

        PtDebugPrint("xEntryCam.__init__() v.%d" % self.version)

    def _DisableEnterRgn(self, reason="unknown"):
        PtDebugPrint("xEntryCam._DisableEnterRgn(): [%s] Disabling entry region... [Reason: %s]" % (self.sceneobject.getName(), reason))

        # We're tracking the enable/disable state of the entry camera region in
        # a variable because disabling the activator seems to propagate out to
        # the host scene object, disabling both regions. We need the exit region
        # to be active, though, to ensure the entry camera is popped off on exit,
        # not when we link-in... that defeats the purpose!
        self._linkedIn = True
        PtGetLocalAvatar().avatar.unRegisterForBehaviorNotify(self.key)

    def OnServerInitComplete(self):
        # When we finish linking in, all entry regions will be disabled.
        PtGetLocalAvatar().avatar.registerForBehaviorNotify(self.key)

    def OnBehaviorNotify(self, type, id, state):
        if type == PtBehaviorTypes.kBehaviorTypeLinkIn:
            # TPotS seems to fire this notification *before* firing the entry cam
            # region collision event. In MOUL, the collision fires before the link
            # effects trigger. So, to make sure we receive our collision notification,
            # delay just a bit.
            PtAtTimeCallback(self.key, 0.1, kDisableRegionTimer)

    def OnTimer(self, id):
        if id == kDisableRegionTimer:
            self._DisableEnterRgn("done linking in")

    def _GetCollisionEvent(self, events):
        for event in events:
            if event[0] == kCollisionEvent:
                return event[1]
        return None

    def OnNotify(self, state, id, events):
        if PtFindAvatar(events) != PtGetLocalAvatar():
            return
        if id != actRegionSensor.id:
            return

        # Search for a collision event in the notification's event records. The second item in
        # the record is the entered state. Note that the engine is currently passing integers
        # instead of proper booleans, and a value of `None` indicates that no collision
        # event was found at all. Be careful!
        rgnEntered = self._GetCollisionEvent(events)

        if rgnEntered is None:
            PtDebugPrint("xEntryCam.OnNotify(): [%s] Ignoring spurious notification" % self.sceneobject.getName())
        elif rgnEntered:
            if self._linkedIn:
                PtDebugPrint("xEntryCam.OnNotify(): [%s] Ignoring entry cam enter trigger" % self.sceneobject.getName())
                return

            self._DisableEnterRgn("entry cam triggered")
            PtDebugPrint("xEntryCam.OnNotify(): [%s] Pushing entry camera %s" % (self.sceneobject.getName(), camera.value.getName()))
            if not self._entryCamActive:
                virtCam = ptCamera()
                virtCam.save(camera.value.getKey())
                # This will only force the player out of first person if the age creator says that we should do so *and* the player
                # has *not* enabled the "stay in first person" flag. If the player has "stay in first person" enabled, then they
                # will not be forced into third person for this entry camera.
                if undoFirstPerson.value:
                    virtCam.undoFirstPerson()
            self._entryCamActive = True
        else:
            if not self._entryCamActive:
                PtDebugPrint("xEntryCam.OnNotify(): %s Ignoring entry cam region exit trigger" % self.sceneobject.getName())
                return

            PtDebugPrint("xEntryCam.OnNotify(): [%s] Popping entry camera %s" % (self.sceneobject.getName(), camera.value.getName()))
            virtCam = ptCamera()
            virtCam.restore(camera.value.getKey())
            self._entryCamActive = False


## Copy'pasta' glue from plasma/glue.py for TPotS. Not needed for Korman.
glue_cl = None
glue_inst = None
glue_params = None
glue_paramKeys = None
try:
    x = glue_verbose
except NameError:
    glue_verbose = 0

def glue_getClass():
    global glue_cl
    if (glue_cl == None):
        try:
            cl = eval(glue_name)
            if issubclass(cl, ptModifier):
                glue_cl = cl
            elif glue_verbose:
                print ('Class %s is not derived from modifier' % cl.__name__)
        except NameError:
            if glue_verbose:
                try:
                    print ('Could not find class %s' % glue_name)
                except NameError:
                    print 'Filename/classname not set!'
    return glue_cl


def glue_getInst():
    global glue_inst
    if (type(glue_inst) == type(None)):
        cl = glue_getClass()
        if (cl != None):
            glue_inst = cl()
    return glue_inst


def glue_delInst():
    global glue_inst
    global glue_cl
    global glue_paramKeys
    global glue_params
    if (type(glue_inst) != type(None)):
        del glue_inst
    glue_cl = None
    glue_params = None
    glue_paramKeys = None


def glue_getVersion():
    inst = glue_getInst()
    ver = inst.version
    glue_delInst()
    return ver


def glue_findAndAddAttribs(obj, glue_params):
    if isinstance(obj, ptAttribute):
        if glue_params.has_key(obj.id):
            if glue_verbose:
                print 'WARNING: Duplicate attribute ids!'
                print ('%s has id %d which is already defined in %s' %
                      (obj.name, obj.id, glue_params[obj.id].name))
        else:
            glue_params[obj.id] = obj
    elif type(obj) == type([]):
        for o in obj:
            glue_findAndAddAttribs(o, glue_params)
    elif type(obj) == type({}):
        for o in obj.values():
            glue_findAndAddAttribs(o, glue_params)
    elif type(obj) == type(()):
        for o in obj:
            glue_findAndAddAttribs(o, glue_params)


def glue_getParamDict():
    global glue_paramKeys
    global glue_params
    if type(glue_params) == type(None):
        glue_params = {}
        gd = globals()
        for obj in gd.values():
            glue_findAndAddAttribs(obj, glue_params)
        glue_paramKeys = glue_params.keys()
        glue_paramKeys.sort()
        glue_paramKeys.reverse()
    return glue_params


def glue_getClassName():
    cl = glue_getClass()
    if (cl != None):
        return cl.__name__
    if glue_verbose:
        print ('Class not found in %s.py' % glue_name)
    return None


def glue_getBlockID():
    inst = glue_getInst()
    if (inst != None):
        return inst.id
    if glue_verbose:
        print ('Instance could not be created in %s.py' % glue_name)
    return None


def glue_getNumParams():
    pd = glue_getParamDict()
    if (pd != None):
        return len(pd)
    if glue_verbose:
        print ('No attributes found in %s.py' % glue_name)
    return 0


def glue_getParam(number):
    pd = glue_getParamDict()
    if (pd != None):
        if type(glue_paramKeys) == type([]):
            if (number >= 0) and (number < len(glue_paramKeys)):
                return pd[glue_paramKeys[number]].getdef()
            else:
                print ('glue_getParam: Error! %d out of range of attribute list' % number)
        else:
            pl = pd.values()
            if (number >= 0) and (number < len(pl)):
                return pl[number].getdef()
            elif glue_verbose:
                print ('glue_getParam: Error! %d out of range of attribute list' % number)
    if glue_verbose:
        print 'GLUE: Attribute list error'
    return None


def glue_setParam(id, value):
    pd = glue_getParamDict()
    if (pd != None):
        if pd.has_key(id):
            try:
                pd[id].__setvalue__(value)
            except AttributeError:
                if isinstance(pd[id], ptAttributeList):
                    try:
                        if type(pd[id].value) != type([]):
                            pd[id].value = []
                    except AttributeError:
                        pd[id].value = []
                    pd[id].value.append(value)
                else:
                    pd[id].value = value
        elif glue_verbose:
            print "setParam: can't find id=", id
    else:
        print 'setParma: Something terribly has gone wrong. Head for the cover.'


def glue_isNamedAttribute(id):
    pd = glue_getParamDict()
    if (pd != None):
        try:
            if isinstance(pd[id], ptAttribNamedActivator):
                return 1
            if isinstance(pd[id], ptAttribNamedResponder):
                return 2
        except KeyError:
            if glue_verbose:
                print ('Could not find id=%d attribute' % id)
    return 0


def glue_isMultiModifier():
    inst = glue_getInst()
    if isinstance(inst, ptMultiModifier):
        return 1
    return 0


def glue_getVisInfo(number):
    pd = glue_getParamDict()
    if pd != None:
        if type(glue_paramKeys) == type([]):
            if (number >= 0) and (number < len(glue_paramKeys)):
                return pd[glue_paramKeys[number]].getVisInfo()
            else:
                print ('glue_getVisInfo: Error! %d out of range of attribute list' % number)
        else:
            pl = pd.values()
            if (number >= 0) and (number < len(pl)):
                return pl[number].getVisInfo()
            elif glue_verbose:
                print ('glue_getVisInfo: Error! %d out of range of attribute list' % number)
    if glue_verbose:
        print 'GLUE: Attribute list error'
    return None
