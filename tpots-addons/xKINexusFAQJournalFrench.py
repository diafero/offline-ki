# -*- coding: utf-8 -*-
#==============================================================================#
#                                                                              #
#    This is a patched file that was originally written by Cyan Worlds Inc.    #
#    See the file AUTHORS for more info about the contributors of the changes  #
#                                                                              #
#    This program is distributed in the hope that it will be useful,           #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                      #
#                                                                              #
#    You may re-use the code in this file within the context of Uru.           #
#                                                                              #
#==============================================================================#
xJournalContents = """<cover src="xKINexusFAQJournalCover*1#0.hsm"><font size=10 face=Courier color=982A2A><margin left=62 right=62 top=48>

Suite aux nombreuses demandes d'informations concernant le fonctionnement du KI et du Nexus, le CRD a d\xe9cid\xe9 de publier ce manuel. Nous remercions la Guilde des Greeters pour tous les efforts accomplis dans l'\xe9criture de ce manuel. Nous appr\xe9cions cel\xe0 !

S'il vous pla\xeet, soyez conscients que bien que des zones et/ou des actions soient d\xe9crites dans ce manuel, le CRD ne les approuve pas. Il existe des zones que le CRD consid\xe8re comme dangereuses et certaines actions qu'il n'a pas approuv\xe9es. Ces zones ne peuvent pas \xeatre contr\xf4l\xe9es pour l'instant.

Nous esp\xe9rons n\xe9anmoins que ces informations vous seront utiles.

Amicalement,

Le CRD.


LE KI

Par commodit\xe9, j'appellerai mini KI l'image du KI qui se trouve en haut \xe0 gauche du jeu et maxi KI l'\xe9cran qui appara\xeet lorsqu'on ouvre le mini KI.

En cliquant sur l'ic\xf4ne du livre au centre du mini KI ou en pressant la touche F6 de votre clavier, vous cr\xe9erez un nouveau message. En cliquant sur le cercle blanc au-dessus de l'ic\xf4ne du livre ou en pressant la touche F5, vous prendrez une photo. Le cercle blanc est suppos\xe9 repr\xe9senter un oeil, cela dans la continuit\xe9 du th\xe8me de la Guilde du personnel d'entretien (maintainers). Vous pourrez voir leur symbole \xe0 de nombreux endroits si vous cherchez bien. Pour supprimer un message, une photo ou un ami de votre liste, cliquez sur ce message, cette photo ou cet ami. Une fen\xeatre va s'ouvrir dans votre maxi KI\xa0: cliquez le z\xe9ro D'ni qui se trouve dans cette fen\xeatre, sur la droite.

Sur le c\xf4t\xe9 droit de votre mini KI vous verrez plusieurs cercles\xa0: ce sont des boutons de commandes qui ont diverses fonctions et dans l'ordre de haut en bas: le premier bouton sert \xe0 ouvrir/fermer le maxi KI, le second \xe0 allumer/\xe9teindre le son et le troisi\xe8me \xe0 montrer si votre conversation est publique ou priv\xe9e. Tout en bas du mini KI il y a un tout petit cercle blanc qui permet de fermer compl\xe8tement le KI lorsqu'on clique dessus. Sur la gauche du mini KI il y a un double cercle lumineux qui repr\xe9sente la fonction de calibration. En fait ce cercle a deux fonctions qui ne sont pas utilisables pour l'instant: la premi\xe8re fonction sert \xe0 montrer \xe0 quoi ressembleront les marqueurs quand vous jouerez un jeu de marqueurs, la seconde fonction correspond \xe0 la ligne qui se d\xe9place entre le milieu et le haut du cercle lumineux: cette ligne indique la qualit\xe9 de la transmission des informations du jeu entre les serveurs et vous-m\xeame. Si cette ligne ne bouge pas c'est que vous ne bougez pas.


Comment inviter une personne dans l'un de mes \xc2ges\xa0?

C'est un proc\xe9d\xe9 un peu compliqu\xe9 qui peut \xeatre fait de diverses mani\xe8res. L'une d'elle est de partager son livre Relto avec la personne que l'on veut inviter. Pour ce faire, vous ouvrez votre livre Relto, vous cliquez sur la page gauche \xab\xa0partager votre livre\xa0\xbb et ensuite vous cliquez le petit curseur du livre qui va appara\xeetre sur la personne avec qui vous voulez partager votre livre. Si vous n'y arrivez pas la premi\xe8re fois, faites une nouvelle tentative mais surtout ne cliquez pas de fa\xe7on rapide et successive sur cette personne, cela ne vous sera d'aucune aide, bien au contraire. Une fois que votre ami aura \xe9t\xe9 t\xe9l\xe9port\xe9 dans votre Relto, suivez-le et allez devant la biblioth\xe8que. Basculez la languette en m\xe9tal qui se trouve juste au-dessus du livre de l'\xc2ge que vous voulez partager. Cela fait, votre ami pourra utiliser votre livre et sera t\xe9l\xe9port\xe9 dans votre \xc2ge. Bien entendu, il est possible de basculer cette languette avant m\xeame de faire venir votre ami dans votre Relto.

Une autre mani\xe8re d'inviter quelqu'un dans l'un de vos \xc2ges est d'ouvrir votre maxi KI et de s\xe9lectionner l'ic\xf4ne du milieu (ic\xf4ne d'un joueur). Ensuite, vous localisez le nom du joueur que vous voulez inviter et vous cliquez sur son nom. Vous pourrez trouver le nom de ce joueur, soit dans la liste sur la gauche de votre \xe9cran sous le mini KI, si le joueur est en ligne, soit en cliquant sur les rubriques qui se trouvent sous l'ic\xf4ne du milieu dans votre maxi KI jusqu'\xe0 ce que vous trouviez le nom du joueur parmi les noms qui s'afficheront sur la droite de la fen\xeatre. Une fois que vous avez trouv\xe9 le nom, cliquez dessus et il devrait alors appara\xeetre dans la petite case au dessus de l'ic\xf4ne. Devraient aussi appara\xeetre des petits triangles pointant sur certaines rubriques sous l'ic\xf4ne. Faites d\xe9filer la liste des rubriques jusqu'\xe0 ce que vous trouviez le nom de l'\xc2ge pour lequel vous voulez envoyer une invitation au joueur que vous avez s\xe9lectionn\xe9 et cliquez sur le triangle se trouvant devant cet \xc2ge. Si le triangle dispara\xeet c'est que l'invitation a \xe9t\xe9 bien re\xe7ue.


Comment ajouter une personne \xe0 ma liste d'amis\xa0?

Ouvrez la fen\xeatre principale du maxi KI et s\xe9lectionnez l'ic\xf4ne du milieu (ic\xf4ne d'un joueur). Trouvez le nom du joueur que vous voulez ajouter \xe0 votre liste d'amis et cliquez dessus. Vous pouvez cliquer sur ce nom, soit dans la liste sur la gauche de votre \xe9cran sous le mini KI, si le joueur est en ligne, soit sur l'une des rubriques qui se trouvent sous l'ic\xf4ne du milieu dans le maxi KI jusqu'\xe0 ce que vous trouviez le nom du joueur parmi les noms qui s'afficheront sur la droite de la fen\xeatre.

Quand vous trouverez le nom que vous cherchez, cliquez dessus et ce nom appara\xeetra dans la petite case au-dessus des ic\xf4nes. Des petits triangles appara\xeetront pointant sur certaines rubriques.

Cliquez le triangle pointant sur la rubrique amis. Le nom que vous avez s\xe9lectionn\xe9 appara\xeetra dans la liste tout \xe0 gauche sous le mini KI dans la rubrique amis \xe0 chaque fois que cette personne sera en ligne.

Une autre mani\xe8re de faire est d'ouvrir la fen\xeatre principale du maxi KI, de cliquer l'ic\xf4ne du milieu et de s\xe9lectionner la rubrique \xab\xa0amis\xa0\xbb. Une fen\xeatre s'ouvrira avec en haut le texte suivant\xa0: \xab\xa0Add a buddy by ID\xa0\xbb. Tapez le num\xe9ro du KI ou le nom de la personne que vous voulez ajouter \xe0 votre liste d'amis. Une fois que vous aurez pressez la touche entr\xe9e de votre clavier la personne devrait appara\xeetre dans votre liste d'amis.


Comment devenir membre d'un quartier\xa0?

Il faut qu'un membre de ce quartier vous invite dans son Relto pour que vous utilisiez son livre de quartier sachant qu'avant, il faut que vous ayiez d\xe9truit le livre de quartier qui se trouve dans votre propre Relto. C'est la seule mani\xe8re de devenir membre d'un quartier, mais pas la seule mani\xe8re de visiter un quartier (cf explication ci-dessus \xab\xa0Comment inviter une personne dans l'un de mes \xc2ges\xa0\xbb).

Un ami import\xe9 dans le jeu en utilisant la commande /inviter deviendra \xe9galement membre de votre quartier. Voir les \xab\xa0commandes d'invitation\xa0\xbb.<pb>Qu'est-ce qu'un marqueur\xa0?

Un marqueur est un \xe9l\xe9ment du KI qui a plusieurs fonctions. Les marqueurs ressemblent \xe0 des petites sph\xe8res lumineuses qui flottent et peuvent \xeatre utilis\xe9s dans des jeux comme la chasse aux oeufs de P\xe2ques (partie de capture), la capture du drapeau (partie de d\xe9fense) ou la chasse au papier (qu\xeate\xa0: A conduit \xe0 B, B \xe0 C)

Pour cr\xe9er un jeu de marqueurs, vous devez d'abord calibrer votre KI en effectuant les missions officielles de marqueurs. Une fois votre KI calibr\xe9, vous pressez la touche F8 de votre clavier ou tapez la commande /createmarkerfolder. La fen\xeatre du maxi KI va s'ouvrir sur une fen\xeatre de cr\xe9ation de jeu. Cliquez sur Modifier la partie et choisir le type de jeu que vous voulez cr\xe9er (capture, d\xe9fense ou qu\xeate). Si vous s\xe9lectionner une partie de capture ou de d\xe9fense vous devrez d\xe9finir une limite de temps pour jouer le jeu. Sans cliquer sur Modification termin\xe9e, fermez la fen\xeatre de votre maxi KI (pressez F2 ou cliquez sur le bouton en haut \xe0 droite du mini KI). Promenez-vous dans l'\xc2ge et placez des marqueurs aux endroits qui vous plaisent. Pour placer un marqueur pressez F7 (vous pouvez placer des marqueurs \xe0 n'importe quel endroit accessible). Quand vous avez plac\xe9 tous les marqueurs que vous d\xe9sirez, r\xe9ouvrez le maxi KI et cliquez sur Liste des marqueurs. Vous verrez alors appara\xeetre tous les marqueurs que vous avez plac\xe9s. Si vous voulez, vous pouvez changer le nom des marqueurs plac\xe9s ou les supprimer. Vous ne pouvez pas changer le nom d'un marqueur ou supprimer un marqueur qui se trouve dans un autre \xc2ge que celui ou vous \xeates. Quand vous \xeatre satisfait de votre jeu, cliquez sur Modification termin\xe9e et ensuite sur Jouer.

Si vous avez choisi un jeu de capture ou de d\xe9fense vous pouvez inviter d'autres personnes \xe0 jouer avec vous. Vous pouvez soit inviter toutes les personnes qui se trouvent dans l'\xc2ge, soit des personnes une par une. Quand de nouvelles personnes se joignent au jeu elles sont divis\xe9es en deux \xe9quipes et appara\xeetront sur le KI de tous les participants.

Si vous avez choisi de cr\xe9er une qu\xeate, cliquez sur Jouer pour commencer \xe0 jouer. Vous pouvez envoyer le jeu \xe0 vos amis. Un affichage vous montrera les marqueurs que vous avez trouv\xe9s et ceux qu'il vous reste \xe0 trouver.


Combien de photos puis-je stocker dans mon KI\xa0?

Environ 15 photos ou messages. Ce nombre pourrait \xeatre augment\xe9 dans le futur.


Comment placer une photo dans l'imageur\xa0?

D'abord vous devez \xeatre membre du quartier o\xf9 se trouve l'imageur. Ouvrez la fen\xeatre principale du maxi KI et s\xe9lectionnez la photo que vous voulez ajouter \xe0 l'imageur. L'imageur devrait appara\xeetre dans la liste des rubriques sur la gauche de votre \xe9cran de jeu sous le mini KI ou sur la liste du milieu du maxi KI sous les trois ic\xf4nes. S\xe9lectionnez l'imageur qui doit appara\xeetre dans la case au dessus des ic\xf4nes et cliquez sur le triangle qui appara\xeet \xe0 c\xf4t\xe9 de la photo ou du message que vous avez s\xe9lectionn\xe9. La photo ou le message va appara\xeetre sur l'imageur.


Comment supprimer ou r\xe9initialiser un \xc2ge\xa0?

Dans la hutte de votre Relto, cliquez sur votre biblioth\xe8que. Si vous regardez vos livres vous verrez qu'il y a une languette en m\xe9tal en haut et en bas de chaque livre. La languette du haut, quand elle est bascul\xe9e, permet aux autres joueurs d'utiliser vos livres et de visiter vos \xc2ges. Si vous cliquez sur la languette du bas vous supprimerez ou r\xe9initialiserez un \xc2ge.
SOYEZ SUR DE VOUS QUAND VOUS SUPPRIMEZ OU REINITIALISEZ UN AGE CAR IL N'Y A PAS DE RETOUR EN ARRIERE POSSIBLE.


Comment cr\xe9er un nouveau message\xa0?

Ouvrez le mini KI et cliquez sur l'ic\xf4ne qui se trouve au centre et qui a l'apparence d'un livre ouvert. Le maxi KI va alors s'ouvrir et faire appara\xeetre la fen\xeatre de message.

Une autre m\xe9thode est de presser la touche F6 du clavier.

La fonction message peut \xeatre tr\xe8s utile pour stocker des notes sur le d\xe9roulement de votre exploration dans D'ni et peut aussi \xeatre utilis\xe9e comme un e-mail appel\xe9 aussi KI-mail (c'est un jeu de mot, \xab\xa0KI-mail\xa0\xbb\xa0? Vous comprenez? \xab\xa0Qui-Mail\xa0\xbb\xa0? Oh ce n'est pas grave). Ecrivez un message et ensuite envoyez-le \xe0 vos amis en utilisant la m\xe9thode d\xe9crite plus loin.


Comment prendre une photo\xa0?

Placez-vous \xe0 l'endroit o\xf9 vous voulez prendre la photo. Si vous ne voulez pas \xeatre sur la photo, basculez en premi\xe8re personne en pressant la touche F1 de votre clavier ou en mettant le curseur de la souris sur votre avatar pour devenir transparent (ce n'est cependant pas parfait, vous \xeates transparent mais pas compl\xe8tement invisible).

Activez votre KI pour qu'il se trouve en haut \xe0 gauche de votre \xe9cran (mini KI) et cliquez sur le petit cercle en haut, au dessus de l'ic\xf4ne du livre ouvert ou pressez la touche F5 de votre clavier. Vous entendrez alors un bruit d'appareil photo et le maxi KI s'ouvrira avec votre ph'to. Vous pouvez alors changer le nom de cette photo, la placer dans une autre rubrique de votre KI ou en envoyer une copie \xe0 un ami.


Comment envoyer quelque chose \xe0 une autre personne\xa0?

S\xe9lectionnez le nom de la personne vers laquelle vous souhaitez faire le transfert (Vous pouvez faire cela de diff\xe9rentes mani\xe8res mais vous saurez que vous l'avez fait correctement lorsque le nom de la personne s'affichera dans la petite case au dessus des trois ic\xf4nes).

S\xe9lectionnez ensuite la chose que vous d\xe9sirez envoyer \xe0 cette personne. Vous devriez alors voir appara\xeetre un petit triangle \xe0 c\xf4t\xe9 de la case o\xf9 se trouve le nom de la personne s\xe9lectionn\xe9e. Cliquez sur ce triangle et quand il dispara\xeet cela signifie que le transfert a \xe9t\xe9 fait.

Si vous cliquez sur les triangles se trouvant devant les autres rubriques, vous transf\xe9rerez cette chose dans ces rubriques.



LE NEXUS

Le Nexus est un grand espace de stockage de livres de liaison construit pas les D'ni. Il sert de carrefour de t\xe9l\xe9portation.
On peut se t\xe9l\xe9porter dans le Nexus par le livre qui se trouve sur le pi\xe9destal dans le quartier (approuv\xe9 par le CRD) ou par le livre du Nexus qui se trouve dans la biblioth\xe8que du Relto.


Comment utiliser le Nexus\xa0?

Une fois dans le Nexus, introduisez votre main dans le r\xe9ceptacle du KI en cliquant sur la petite fente sur la gauche de la machine.


Comment se t\xe9l\xe9porter dans un lieu donn\xe9\xa0?

S\xe9lectionnez dans le Nexus le lieu o\xf9 vous d\xe9sirez vous rendre, pressez le gros bouton tout en haut de la fen\xeatre Nexus et activez le panneau de t\xe9l\xe9portation du livre qui appara\xeet.


Quelle est la diff\xe9rence entre les liaisons publiques et priv\xe9es\xa0?

Une liaison publique est une liaison ouverte \xe0 tout le monde. Une liaison priv\xe9e n'est ouverte que si vous avez re\xe7u une invitation par un ami.

Certaines zones peuvent \xeatre rendues priv\xe9es ou publiques en cliquant sur le bouton \xab\xa0rendre public\xa0\xbb ou \xab\xa0rendre priv\xe9\xa0\xbb en haut \xe0 droite de la fen\xeatre du Nexus.


Que sont les liaisons personnelles\xa0?

A chaque fois que vous vous t\xe9l\xe9portez pour la premi\xe8re fois dans un \xc2ge par l'interm\xe9diaire des livres de votre Relto ou des livres approuv\xe9s par le CRD, une nouvelle liaison s'inscrit dans votre Nexus. Ces liaisons personnelles peuvent \xeatre partag\xe9es avec d'autres joueurs et, une fois partag\xe9e par l'un de vos amis, ces liaisons appara\xeetront dans la section liaison personnelle de votre Nexus.


Quelles sont les diverses zones de la ville\xa0?

Les zones de la ville sont les points de liaison o\xf9 vous pouvez vous t\xe9l\xe9porter dans Ae'gura ou dans la ville elle-m\xeame (\xe0 ajouter plus tard). Vous pouvez ajouter certaines de ces liaisons en enregistrant votre KI dans les divers pi\xe9destaux que vous trouverez dans la ville. D'autres liaisons pour la ville sont ajout\xe9es par le CRD quand la zone est approuv\xe9e.


Puis-je trier mes liaisons\xa0?

Les liaisons peuvent \xeatre tri\xe9es par nom ou par population en cliquant sur l'un de ces deux mots dans la fen\xeatre Nexus. La fl\xe8che \xe0 c\xf4t\xe9 de chacun de ces mots permet de trier les listes dans l'ordre inverse.


Comment cr\xe9er un quartier\xa0?

Vous devez d'abord vous assurer que vous avez effac\xe9 votre livre de quartier dans votre Relto en cliquant sur la languette en bas du livre pour le faire dispara\xeetre en retrait. On vous demandera si vous d\xe9sirez annuler votre inscription dans ce quartier et il faudra r\xe9pondre oui. Apr\xe8s avoir cliqu\xe9 sur oui, t\xe9l\xe9portez-vous dans votre Nexus et glissez votre main dans la fente en cliquant dessus. Vous verrez l'ic\xf4ne d'un livre clignotant appara\xeetre en haut \xe0 gauche. Cliquez sur cet ic\xf4ne, cela vous permettra de cr\xe9er un nouveau quartier dont vous deviendrez membre. On vous demandera si vous \xeates s\xfbr de vouloir continuer, cliquez sur OUI. Vous \xeates maintenant le fondateur d''n nouveau quartier et vous pouvez inviter tous vos amis \xe0 en devenir membres par l'interm\xe9diaire de votre KI, et rendre ce quartier public ou priv\xe9.

Si un quartier n'a pas de membre il sera d\xe9finitivement supprim\xe9.
"""


# Unescaped content for reference:
xJournalContentsUnescaped = """<cover src="xKINexusFAQJournalCover*1#0.hsm"><font size=10 face=Courier color=982A2A><margin left=62 right=62 top=48>

Suite aux nombreuses demandes d'informations concernant le fonctionnement du KI et du Nexus, le CRD a décidé de publier ce manuel. Nous remercions la Guilde des Greeters pour tous les efforts accomplis dans l'écriture de ce manuel. Nous apprécions celà !

S'il vous plaît, soyez conscients que bien que des zones et/ou des actions soient décrites dans ce manuel, le CRD ne les approuve pas. Il existe des zones que le CRD considère comme dangereuses et certaines actions qu'il n'a pas approuvées. Ces zones ne peuvent pas être contrôlées pour l'instant.

Nous espérons néanmoins que ces informations vous seront utiles.

Amicalement,

Le CRD.


LE KI

Par commodité, j'appellerai mini KI l'image du KI qui se trouve en haut à gauche du jeu et maxi KI l'écran qui apparaît lorsqu'on ouvre le mini KI.

En cliquant sur l'icône du livre au centre du mini KI ou en pressant la touche F6 de votre clavier, vous créerez un nouveau message. En cliquant sur le cercle blanc au-dessus de l'icône du livre ou en pressant la touche F5, vous prendrez une photo. Le cercle blanc est supposé représenter un oeil, cela dans la continuité du thème de la Guilde du personnel d'entretien (maintainers). Vous pourrez voir leur symbole à de nombreux endroits si vous cherchez bien. Pour supprimer un message, une photo ou un ami de votre liste, cliquez sur ce message, cette photo ou cet ami. Une fenêtre va s'ouvrir dans votre maxi KI\u00A0: cliquez le zéro D'ni qui se trouve dans cette fenêtre, sur la droite.

Sur le côté droit de votre mini KI vous verrez plusieurs cercles\u00A0: ce sont des boutons de commandes qui ont diverses fonctions et dans l'ordre de haut en bas: le premier bouton sert à ouvrir/fermer le maxi KI, le second à allumer/éteindre le son et le troisième à montrer si votre conversation est publique ou privée. Tout en bas du mini KI il y a un tout petit cercle blanc qui permet de fermer complètement le KI lorsqu'on clique dessus. Sur la gauche du mini KI il y a un double cercle lumineux qui représente la fonction de calibration. En fait ce cercle a deux fonctions qui ne sont pas utilisables pour l'instant: la première fonction sert à montrer à quoi ressembleront les marqueurs quand vous jouerez un jeu de marqueurs, la seconde fonction correspond à la ligne qui se déplace entre le milieu et le haut du cercle lumineux: cette ligne indique la qualité de la transmission des informations du jeu entre les serveurs et vous-même. Si cette ligne ne bouge pas c'est que vous ne bougez pas.


Comment inviter une personne dans l'un de mes Âges\u00A0?

C'est un procédé un peu compliqué qui peut être fait de diverses manières. L'une d'elle est de partager son livre Relto avec la personne que l'on veut inviter. Pour ce faire, vous ouvrez votre livre Relto, vous cliquez sur la page gauche \xab\xa0\u00A0partager votre livre\u00A0\xa0\xbb et ensuite vous cliquez le petit curseur du livre qui va apparaître sur la personne avec qui vous voulez partager votre livre. Si vous n'y arrivez pas la première fois, faites une nouvelle tentative mais surtout ne cliquez pas de façon rapide et successive sur cette personne, cela ne vous sera d'aucune aide, bien au contraire. Une fois que votre ami aura été téléporté dans votre Relto, suivez-le et allez devant la bibliothèque. Basculez la languette en métal qui se trouve juste au-dessus du livre de l'Âge que vous voulez partager. Cela fait, votre ami pourra utiliser votre livre et sera téléporté dans votre Âge. Bien entendu, il est possible de basculer cette languette avant même de faire venir votre ami dans votre Relto.

Une autre manière d'inviter quelqu'un dans l'un de vos Âges est d'ouvrir votre maxi KI et de sélectionner l'icône du milieu (icône d'un joueur). Ensuite, vous localisez le nom du joueur que vous voulez inviter et vous cliquez sur son nom. Vous pourrez trouver le nom de ce joueur, soit dans la liste sur la gauche de votre écran sous le mini KI, si le joueur est en ligne, soit en cliquant sur les rubriques qui se trouvent sous l'icône du milieu dans votre maxi KI jusqu'à ce que vous trouviez le nom du joueur parmi les noms qui s'afficheront sur la droite de la fenêtre. Une fois que vous avez trouvé le nom, cliquez dessus et il devrait alors apparaître dans la petite case au dessus de l'icône. Devraient aussi apparaître des petits triangles pointant sur certaines rubriques sous l'icône. Faites défiler la liste des rubriques jusqu'à ce que vous trouviez le nom de l'Âge pour lequel vous voulez envoyer une invitation au joueur que vous avez sélectionné et cliquez sur le triangle se trouvant devant cet Âge. Si le triangle disparaît c'est que l'invitation a été bien reçue.


Comment ajouter une personne à ma liste d'amis\u00A0?

Ouvrez la fenêtre principale du maxi KI et sélectionnez l'icône du milieu (icône d'un joueur). Trouvez le nom du joueur que vous voulez ajouter à votre liste d'amis et cliquez dessus. Vous pouvez cliquer sur ce nom, soit dans la liste sur la gauche de votre écran sous le mini KI, si le joueur est en ligne, soit sur l'une des rubriques qui se trouvent sous l'icône du milieu dans le maxi KI jusqu'à ce que vous trouviez le nom du joueur parmi les noms qui s'afficheront sur la droite de la fenêtre.

Quand vous trouverez le nom que vous cherchez, cliquez dessus et ce nom apparaîtra dans la petite case au-dessus des icônes. Des petits triangles apparaîtront pointant sur certaines rubriques.

Cliquez le triangle pointant sur la rubrique amis. Le nom que vous avez sélectionné apparaîtra dans la liste tout à gauche sous le mini KI dans la rubrique amis à chaque fois que cette personne sera en ligne.

Une autre manière de faire est d'ouvrir la fenêtre principale du maxi KI, de cliquer l'icône du milieu et de sélectionner la rubrique \xab\xa0\u00A0amis\u00A0\xa0\xbb. Une fenêtre s'ouvrira avec en haut le texte suivant\u00A0: \xab\xa0\u00A0Add a buddy by ID\u00A0\xa0\xbb. Tapez le numéro du KI ou le nom de la personne que vous voulez ajouter à votre liste d'amis. Une fois que vous aurez pressez la touche entrée de votre clavier la personne devrait apparaître dans votre liste d'amis.


Comment devenir membre d'un quartier\u00A0?

Il faut qu'un membre de ce quartier vous invite dans son Relto pour que vous utilisiez son livre de quartier sachant qu'avant, il faut que vous ayiez détruit le livre de quartier qui se trouve dans votre propre Relto. C'est la seule manière de devenir membre d'un quartier, mais pas la seule manière de visiter un quartier (cf explication ci-dessus \xab\xa0\u00A0Comment inviter une personne dans l'un de mes Âges\u00A0\xa0\xbb).

Un ami importé dans le jeu en utilisant la commande /inviter deviendra également membre de votre quartier. Voir les \xab\xa0\u00A0commandes d'invitation\u00A0\xa0\xbb.<pb>Qu'est-ce qu'un marqueur\u00A0?

Un marqueur est un élément du KI qui a plusieurs fonctions. Les marqueurs ressemblent à des petites sphères lumineuses qui flottent et peuvent être utilisés dans des jeux comme la chasse aux oeufs de Pâques (partie de capture), la capture du drapeau (partie de défense) ou la chasse au papier (quête\u00A0: A conduit à B, B à C)

Pour créer un jeu de marqueurs, vous devez d'abord calibrer votre KI en effectuant les missions officielles de marqueurs. Une fois votre KI calibré, vous pressez la touche F8 de votre clavier ou tapez la commande /createmarkerfolder. La fenêtre du maxi KI va s'ouvrir sur une fenêtre de création de jeu. Cliquez sur Modifier la partie et choisir le type de jeu que vous voulez créer (capture, défense ou quête). Si vous sélectionner une partie de capture ou de défense vous devrez définir une limite de temps pour jouer le jeu. Sans cliquer sur Modification terminée, fermez la fenêtre de votre maxi KI (pressez F2 ou cliquez sur le bouton en haut à droite du mini KI). Promenez-vous dans l'Âge et placez des marqueurs aux endroits qui vous plaisent. Pour placer un marqueur pressez F7 (vous pouvez placer des marqueurs à n'importe quel endroit accessible). Quand vous avez placé tous les marqueurs que vous désirez, réouvrez le maxi KI et cliquez sur Liste des marqueurs. Vous verrez alors apparaître tous les marqueurs que vous avez placés. Si vous voulez, vous pouvez changer le nom des marqueurs placés ou les supprimer. Vous ne pouvez pas changer le nom d'un marqueur ou supprimer un marqueur qui se trouve dans un autre Âge que celui ou vous êtes. Quand vous être satisfait de votre jeu, cliquez sur Modification terminée et ensuite sur Jouer.

Si vous avez choisi un jeu de capture ou de défense vous pouvez inviter d'autres personnes à jouer avec vous. Vous pouvez soit inviter toutes les personnes qui se trouvent dans l'Âge, soit des personnes une par une. Quand de nouvelles personnes se joignent au jeu elles sont divisées en deux équipes et apparaîtront sur le KI de tous les participants.

Si vous avez choisi de créer une quête, cliquez sur Jouer pour commencer à jouer. Vous pouvez envoyer le jeu à vos amis. Un affichage vous montrera les marqueurs que vous avez trouvés et ceux qu'il vous reste à trouver.


Combien de photos puis-je stocker dans mon KI\u00A0?

Environ 15 photos ou messages. Ce nombre pourrait être augmenté dans le futur.


Comment placer une photo dans l'imageur\u00A0?

D'abord vous devez être membre du quartier où se trouve l'imageur. Ouvrez la fenêtre principale du maxi KI et sélectionnez la photo que vous voulez ajouter à l'imageur. L'imageur devrait apparaître dans la liste des rubriques sur la gauche de votre écran de jeu sous le mini KI ou sur la liste du milieu du maxi KI sous les trois icônes. Sélectionnez l'imageur qui doit apparaître dans la case au dessus des icônes et cliquez sur le triangle qui apparaît à côté de la photo ou du message que vous avez sélectionné. La photo ou le message va apparaître sur l'imageur.


Comment supprimer ou réinitialiser un Âge\u00A0?

Dans la hutte de votre Relto, cliquez sur votre bibliothèque. Si vous regardez vos livres vous verrez qu'il y a une languette en métal en haut et en bas de chaque livre. La languette du haut, quand elle est basculée, permet aux autres joueurs d'utiliser vos livres et de visiter vos Âges. Si vous cliquez sur la languette du bas vous supprimerez ou réinitialiserez un Âge.
SOYEZ SUR DE VOUS QUAND VOUS SUPPRIMEZ OU REINITIALISEZ UN AGE CAR IL N'Y A PAS DE RETOUR EN ARRIERE POSSIBLE.


Comment créer un nouveau message\u00A0?

Ouvrez le mini KI et cliquez sur l'icône qui se trouve au centre et qui a l'apparence d'un livre ouvert. Le maxi KI va alors s'ouvrir et faire apparaître la fenêtre de message.

Une autre méthode est de presser la touche F6 du clavier.

La fonction message peut être très utile pour stocker des notes sur le déroulement de votre exploration dans D'ni et peut aussi être utilisée comme un e-mail appelé aussi KI-mail (c'est un jeu de mot, \xab\xa0\u00A0KI-mail\u00A0\xa0\xbb\u00A0? Vous comprenez? \xab\xa0\u00A0Qui-Mail\u00A0\xa0\xbb\u00A0? Oh ce n'est pas grave). Ecrivez un message et ensuite envoyez-le à vos amis en utilisant la méthode décrite plus loin.


Comment prendre une photo\u00A0?

Placez-vous à l'endroit où vous voulez prendre la photo. Si vous ne voulez pas être sur la photo, basculez en première personne en pressant la touche F1 de votre clavier ou en mettant le curseur de la souris sur votre avatar pour devenir transparent (ce n'est cependant pas parfait, vous êtes transparent mais pas complètement invisible).

Activez votre KI pour qu'il se trouve en haut à gauche de votre écran (mini KI) et cliquez sur le petit cercle en haut, au dessus de l'icône du livre ouvert ou pressez la touche F5 de votre clavier. Vous entendrez alors un bruit d'appareil photo et le maxi KI s'ouvrira avec votre ph'to. Vous pouvez alors changer le nom de cette photo, la placer dans une autre rubrique de votre KI ou en envoyer une copie à un ami.


Comment envoyer quelque chose à une autre personne\u00A0?

Sélectionnez le nom de la personne vers laquelle vous souhaitez faire le transfert (Vous pouvez faire cela de différentes manières mais vous saurez que vous l'avez fait correctement lorsque le nom de la personne s'affichera dans la petite case au dessus des trois icônes).

Sélectionnez ensuite la chose que vous désirez envoyer à cette personne. Vous devriez alors voir apparaître un petit triangle à côté de la case où se trouve le nom de la personne sélectionnée. Cliquez sur ce triangle et quand il disparaît cela signifie que le transfert a été fait.

Si vous cliquez sur les triangles se trouvant devant les autres rubriques, vous transférerez cette chose dans ces rubriques.



LE NEXUS

Le Nexus est un grand espace de stockage de livres de liaison construit pas les D'ni. Il sert de carrefour de téléportation.
On peut se téléporter dans le Nexus par le livre qui se trouve sur le piédestal dans le quartier (approuvé par le CRD) ou par le livre du Nexus qui se trouve dans la bibliothèque du Relto.


Comment utiliser le Nexus\u00A0?

Une fois dans le Nexus, introduisez votre main dans le réceptacle du KI en cliquant sur la petite fente sur la gauche de la machine.


Comment se téléporter dans un lieu donné\u00A0?

Sélectionnez dans le Nexus le lieu où vous désirez vous rendre, pressez le gros bouton tout en haut de la fenêtre Nexus et activez le panneau de téléportation du livre qui apparaît.


Quelle est la différence entre les liaisons publiques et privées\u00A0?

Une liaison publique est une liaison ouverte à tout le monde. Une liaison privée n'est ouverte que si vous avez reçu une invitation par un ami.

Certaines zones peuvent être rendues privées ou publiques en cliquant sur le bouton \xab\xa0\u00A0rendre public\u00A0\xa0\xbb ou \xab\xa0\u00A0rendre privé\u00A0\xa0\xbb en haut à droite de la fenêtre du Nexus.


Que sont les liaisons personnelles\u00A0?

A chaque fois que vous vous téléportez pour la première fois dans un Âge par l'intermédiaire des livres de votre Relto ou des livres approuvés par le CRD, une nouvelle liaison s'inscrit dans votre Nexus. Ces liaisons personnelles peuvent être partagées avec d'autres joueurs et, une fois partagée par l'un de vos amis, ces liaisons apparaîtront dans la section liaison personnelle de votre Nexus.


Quelles sont les diverses zones de la ville\u00A0?

Les zones de la ville sont les points de liaison où vous pouvez vous téléporter dans Ae'gura ou dans la ville elle-même (à ajouter plus tard). Vous pouvez ajouter certaines de ces liaisons en enregistrant votre KI dans les divers piédestaux que vous trouverez dans la ville. D'autres liaisons pour la ville sont ajoutées par le CRD quand la zone est approuvée.


Puis-je trier mes liaisons\u00A0?

Les liaisons peuvent être triées par nom ou par population en cliquant sur l'un de ces deux mots dans la fenêtre Nexus. La flèche à côté de chacun de ces mots permet de trier les listes dans l'ordre inverse.


Comment créer un quartier\u00A0?

Vous devez d'abord vous assurer que vous avez effacé votre livre de quartier dans votre Relto en cliquant sur la languette en bas du livre pour le faire disparaître en retrait. On vous demandera si vous désirez annuler votre inscription dans ce quartier et il faudra répondre oui. Après avoir cliqué sur oui, téléportez-vous dans votre Nexus et glissez votre main dans la fente en cliquant dessus. Vous verrez l'icône d'un livre clignotant apparaître en haut à gauche. Cliquez sur cet icône, cela vous permettra de créer un nouveau quartier dont vous deviendrez membre. On vous demandera si vous êtes sÃ»r de vouloir continuer, cliquez sur OUI. Vous êtes maintenant le fondateur d''n nouveau quartier et vous pouvez inviter tous vos amis à en devenir membres par l'intermédiaire de votre KI, et rendre ce quartier public ou privé.

Si un quartier n'a pas de membre il sera définitivement supprimé.
"""


